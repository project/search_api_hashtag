Search API Hashtag
------------------

Search API hashtag module provides an index field to store hashtag that appears
in one or more indexed fields. This module is useful to create a facet field to
filter indexed contents by their hashtags.

INSTALLATION
------------

* Install as usual, see http://drupal.org/node/70151 for further information.

USAGE
------------

* Create a Search API index in admin/config/search/search_api/add_index
* Go to Index Filters configuration tab
* Enable and configure "Generate a hashtag indexed field" Data Alteration
* New Hashtag field will be available in the Index field list

CREDITS
------------

* Author: [plopesc](http://drupal.org/user/282415)
* Development sponsored by [Bluespark](http://bluespark.com).